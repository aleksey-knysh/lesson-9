//
//  ViewController.swift
//  HigherOrderFunctions
//
//  Created by Aleksey Knysh on 1/21/21.
//  Copyright © 2021 Tsimafei Harhun. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // создаём словарь имя и телефонный номер человека
        let dictionaryContactNumber = ["Виктор": 5763783, "Евгений": 3243756, "Александр": 8354721, "Генадий": 9010232]
        
        // создаём два масива и заполняем их: отдельно именами и отдельно номерами
        let arrayName = Array(dictionaryContactNumber.keys.map { String($0) })
        let arrayPhone = Array(dictionaryContactNumber.values.map { Int($0) })
        print("\nМассив имён - \(arrayName)\n\nМассив тел. номеров - \(arrayPhone)\n")
        
        // фильтруем массив номеров и выводим номера кратные 3
        let arrayStringPhoneNumbers = arrayPhone.filter { String($0).compactMap { $0.wholeNumberValue }.reduce(0, +); return $0 % 3 == 0 }
        print("Номера, которые делятся на 3 - \(arrayStringPhoneNumbers)")
        
        // Фильтруем массив имён и выводим имена начинающиеся на согласную
        let arrayVowels: Set = ["А", "Е", "И", "O", "У", "Э", "Ю", "Я"]
        let newNames = arrayName.filter({ value in
            guard let firstLetter = value.first else { return false }
            for char in arrayVowels where String(firstLetter) == char {
                return true
            }
            return false
        })
        print("\nИмена, которые начинаются на гласные - \(newNames)")
        
        // Складываем массив тел. номеров
        let sumArrayPhone = arrayPhone.reduce(0, +)
        print("\nСумма всех номеров = \(sumArrayPhone)")
        
        // Выводим имена в алфавитном порядке
        let sorttedArrayName = arrayName.sorted()
        print("\nИмена в алфавитном порядке - \(sorttedArrayName)")
        }
    }
